% TEST_DYNAMIC_GLASSO - Test dynamic GLASSO model
% This function creates synthetic time series data and estimates precision 
% matrices using dynamic GLASSO algorithm
%
% Syntax: [] = test_dynamic_glasso(data, varargin)
%
% Inputs:
%   data - temporal data cell array where each cells has data matrix
%           corresponding to single time point
%   rhopath - a grid of sparsity parameter values from which the
%         optimal one is chosen
%   lambdapath - a grid of smoothing parameter values from which the
%         optimal one is chosen
%   edge_threshold - threshold to remove edges from the graph
%
% Outputs:
%   sol - estimated inverse covariance matrices under optimal parameters
%   rhomax - optimal sparsity parameter among rhopath
%   lambdamax - optimal smoothing parameter among lambdapath
%   loglike - log-likelihood
%   avg_time - average computation time
%
% Other m-files required: Yalmip package
% Subfunctions: none
% MAT-files required: priorConst.mat

% Author: Sunil Kumar, Ph.D., ETH Zurich
% email address: sunil.kumar@alumni.ethz.ch
% Website: https://www.bcs.tu-darmstadt.de/
% June, 2016

%------------------------- BEGIN CODE -------------------------------------
function [] = test_dynamic_glasso(data, varargin)
% % Dynamic Glasso
% Input parameters
% Data - Temporal data in cell formats
% 'saveMatFile' - String, Matlab Data Filename to save output (Default -
% Temp.mat)
% 'TopologyThr' - Real Number, Topology Selection Threshold (Default -
% 0.05)
% 'NumWorkers' - Integer, Parallel pool cluster size (Default - 16)
% 'RhoPath' - Array, Sparsity Parameters
% 'LambdaPath' - Array, Smoothing Parameters
% Ex - DynamicGlasso(Data,'Rhopath',0.5,'LambdaPath',0.1);

% Get arguments
args = varargin;
nargs = length(varargin);
if nargs>0
    for i = 1:2:nargs
        switch args{i}
            case 'saveMatFile', saveMatFile = args{i+1};
            case 'TopologyThr', MS_Thr = args{i+1};
            case 'NumWorkers', NumWorkers = args{i+1};
            case 'RhoPath', rhopath = args{i+1};
            case 'LambdaPath', lambdapath = args{i+1};
        end
    end
end
if ~exist('saveMatFile','var')
    saveMatFile = 'Temp.mat';
end
if ~exist('MS_Thr','var')
    MS_Thr = 0.05;
end
if ~exist('NumWorkers','var')
    NumWorkers = 16;
end

%% Changing Cluster Local Profile
% Increasing number of workers
% (Settings are dependent on server properties)
myCluster = parcluster('local');
myCluster.NumWorkers = NumWorkers;
saveProfile(myCluster);

%% Adding path of YALMIP solver package
addpath(genpath('tbxmanager'));
startup
tbxmanager restorepath

%% Reading Data Properties
numT = length(data);
timePoints = 1:numT;
DataSize = zeros(length(timePoints),1);
NumVar = size(data{1},2);

%% Data Normalization
for i = 1:length(timePoints)
    data{i} = zscore(data{i});
    DataSize(i) = size(data{i},1);
end

%% Generating penalization parameters, if not given as a input
% Parameter Range Choice based on HUGE R-package
S = cell(length(timePoints),1);
for i =1:length(timePoints)
    S{i} = corr(data{i}) - eye(NumVar);
end
par_Ratio = 0.1;
if ~exist('rhopath','var')
    rhopath = 0.5*DataSize(1)*max(abs(S{1}(:)));
    for i = 2:length(timePoints)
        rhopath = max(rhopath,0.5*DataSize(i)*max(abs(S{i}(:))));
    end
    rhopath = exp(linspace(log(par_Ratio*rhopath),log(rhopath),15));
    rhopathmax = rhopath(end);
else
    rhopathmax = nan;
end
if ~exist('lambdapath','var')
    lambdapath = 0;
    for i = 2:length(timePoints)
        lambdapath = max(lambdapath,0.5*max(DataSize(i),DataSize(i-1))*max(abs(S{i}(:)-S{i-1}(:))));
    end
    lambdapath = exp(linspace(log(par_Ratio*lambdapath),log(lambdapath),15));
    lambdapathmax = lambdapath(end);
else
    lambdapathmax = nan;
end
clear S

%% Starting parallel pool if necessary
if length(rhopath)*length(lambdapath) == 1
    poolobj = parpool('local',1);
else
    poolobj = parpool('local',NumWorkers);
end
ms.UseParallel='always'; %#ok<STRNU>

%% Running Dynamic Glasso Algorithm
if isnan(rhopathmax) && isnan(lambdapathmax)
    disp(rhopath);
    disp(lambdapath);
    [sol, rhomax, lambdamax, loglike, avgTime] = dynamic_glasso_bic(data,rhopath,lambdapath,MS_Thr);
    FusedGlassoSoln.sol = sol;
    FusedGlassoSoln.rhomax = rhomax;
    FusedGlassoSoln.lambdamax = lambdamax;
    FusedGlassoSoln.loglike = loglike;
    FusedGlassoSoln.rhopath = rhopath;
    FusedGlassoSoln.lambdapath = lambdapath;
    FusedGlassoSoln.DataType = 'Normalized';
    FusedGlassoSoln.MS_Thr = MS_Thr;
    FusedGlassoSoln.avgTime = avgTime;
    FusedGlassoSolnMoment{1} = FusedGlassoSoln; %#ok<NASGU>
    save(saveMatFile,'FusedGlassoSolnMoment');
else
    flag = 1;
    k = 1;
    while flag
        disp(rhopath);
        disp(lambdapath);
        [sol, rhomax, lambdamax, loglike, avgTime] = dynamic_glasso_bic(data,rhopath,lambdapath,MS_Thr);  % fold = [];
        rhomaxTemp = rhomax;
        lambdamaxTemp = lambdamax;
        
        while rhomax == rhopath(1) || rhomax == rhopath(end) || lambdamax == lambdapath(1) || lambdamax == lambdapath(end)
            % Temporary Saving
            FusedGlassoSoln.sol = sol;
            FusedGlassoSoln.rhomax = rhomax;
            FusedGlassoSoln.lambdamax = lambdamax;
            FusedGlassoSoln.loglike = loglike;
            FusedGlassoSoln.rhopath = rhopath;
            FusedGlassoSoln.lambdapath = lambdapath;
            FusedGlassoSoln.DataType = 'Normalized';
            FusedGlassoSoln.MS_Thr = MS_Thr;
            FusedGlassoSoln.avgTime = avgTime;
            eval(sprintf('FusedGlassoSoln%s = FusedGlassoSoln;',num2str(k)));
            if exist(saveMatFile,'file')
                save(saveMatFile,sprintf('FusedGlassoSoln%s',num2str(k)),'-append');
            else
                save(saveMatFile,sprintf('FusedGlassoSoln%s',num2str(k)));
            end
            k = k+1;
            % %%%%%%%%%%
            fprintf('Expanding range of rho and lambda.....\n');
            if ~isnan(rhopathmax)
                if rhomax == rhopath(1)
                    rhopath = exp(linspace(log(par_Ratio*rhopath(1)),log(rhopath(1)),15));
                elseif rhomax == rhopath(end) && rhomax<rhopathmax
                    rhopath = exp(linspace(log(rhopath(end)),log(rhopath(end)/par_Ratio),15));
                end
            end
            if ~isnan(lambdapathmax)
                if lambdamax == lambdapath(1)
                    lambdapath = exp(linspace(log(par_Ratio*lambdapath(1)),log(lambdapath(1)),15));
                elseif lambdamax == lambdapath(end) && lambdamax<lambdapathmax
                    lambdapath = exp(linspace(log(lambdapath(end)),log(lambdapath(end)/par_Ratio),15));
                end
            end
            
            disp(rhopath);
            disp(lambdapath);
            yalmip('clear');
            [sol, rhomax, lambdamax, loglike,avgTime] = dynamic_glasso_bic(data,rhopath,lambdapath,MS_Thr);    % fold=[]
            if rhomax == rhomaxTemp && lambdamax == lambdamaxTemp
                break;
            else
                rhomaxTemp = rhomax;
                lambdamaxTemp = lambdamax;
            end
        end
        
        flag = 0;
        
        FusedGlassoSoln.sol = sol;
        FusedGlassoSoln.rhomax = rhomax;
        FusedGlassoSoln.lambdamax = lambdamax;
        FusedGlassoSoln.loglike = loglike;
        FusedGlassoSoln.rhopath = rhopath;
        FusedGlassoSoln.lambdapath = lambdapath;
        FusedGlassoSoln.DataType = 'Normalized';
        FusedGlassoSoln.MS_Thr = MS_Thr;
        FusedGlassoSoln.avgTime = avgTime;
        eval(sprintf('FusedGlassoSoln%s = FusedGlassoSoln;',num2str(k)));
        if exist(saveMatFile,'file')
            save(saveMatFile,sprintf('FusedGlassoSoln%s',num2str(k)),'-append');
        else
            save(saveMatFile,sprintf('FusedGlassoSoln%s',num2str(k)));
        end
        k = k+1;
    end
    
    % Local Optimization
    if ~isnan(rhopathmax)
        if rhomax == rhopath(1)
            rhopathA = 0.9333*rhopath(1);  % 14/15
            rhopathB = rhopath(2);
        elseif rhomax == rhopath(end)
            rhopathA = rhopath(end-1);
            if rhomax == rhopathmax
                rhopathB = rhopath(end);
            else
                rhopathB = 1.0667*rhopath(end);     % 16/15
            end
        else
            [~,cnt] = find(rhopath==rhomax);
            rhopathA = rhopath(cnt-1);
            rhopathB = rhopath(cnt+1);
        end
        rhopath = exp(linspace(log(rhopathA),log(rhopathB),15));
    end
    if ~isnan(lambdapathmax)
        if lambdamax == lambdapath(1)
            lambdapathA = 0.933*lambdapath(1);
            lambdapathB = lambdapath(2);
        elseif lambdamax == lambdapath(end)
            lambdapathA = lambdapath(end-1);
            if lambdamax == lambdapathmax
                lambdapathB = lambdapath(end);
            else
                lambdapathB = 1.0667*lambdapath(end);
            end
        else
            [~,cnt] = find(lambdapath==lambdamax);
            lambdapathA = lambdapath(cnt-1);
            lambdapathB = lambdapath(cnt+1);
        end
        lambdapath = exp(linspace(log(lambdapathA),log(lambdapathB),15));
    end
    
    disp(rhopath);
    disp(lambdapath);
    yalmip('clear');
    [sol, rhomax, lambdamax, loglike,avgTime] = dynamic_glasso_bic(data,rhopath,lambdapath,MS_Thr);    % fold=[]
    FusedGlassoSoln.sol = sol;
    FusedGlassoSoln.rhomax = rhomax;
    FusedGlassoSoln.lambdamax = lambdamax;
    FusedGlassoSoln.loglike = loglike;
    FusedGlassoSoln.rhopath = rhopath;
    FusedGlassoSoln.lambdapath = lambdapath;
    FusedGlassoSoln.DataType = 'Normalized';
    FusedGlassoSoln.MS_Thr = MS_Thr;
    FusedGlassoSoln.avgTime = avgTime;
    eval(sprintf('FusedGlassoSoln%s = FusedGlassoSoln;',num2str(k)));
    save(saveMatFile,sprintf('FusedGlassoSoln%s',num2str(k)),'-append');
    k = k+1;
    
    % Add results in singles structure variables
    FusedGlassoSolnMoment = cell(k-1,1); %#ok<NASGU>
    for i = 1:(k-1)
        load(saveMatFile,sprintf('FusedGlassoSoln%s',num2str(i)));
        eval(sprintf('FusedGlassoSolnMoment{%d} = FusedGlassoSoln%s;',i,num2str(i)));
    end
    save(saveMatFile,'FusedGlassoSolnMoment','-append');
end
delete(poolobj);
