% POSTERIOR_SAMPLING - Sampling Based on Paper "The Bayesian Covariance 
%       Lasso : Zakaria Khondker"
% Metropolis Hasting Samping within Gibbs
% This function eatimates precision matrices using Gibbs sampling 
% approach (only applicable to small dataset).
% 
% Syntax: posterior_sampling
%
% Inputs:
%
% Outputs:
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: TimeSeq3TP5V.mat, priorConst.mat

% Author: Sunil Kumar, Ph.D., ETH Zurich
% email address: sunil.kumar@alumni.ethz.ch
% Website: https://www.bcs.tu-darmstadt.de/
% June, 2016

% Load data
load('TimeSeq3TP5V_2.mat','Data','timePoints','NumVar');
load('priorConst.mat','sol3');
rng('shuffle')
num_time = length(Data);
data_size = zeros(num_time, 1);
for i = 1:num_time
    data_size(i) = size(Data{i}, 1);
end
lambda_min = 1;
rho_min = 0.1;

% Finding range of penalty parameters
S = cell(num_time, 1);
for i =1:num_time
    S{i} = corr(Data{i}) - eye(NumVar);
end
rho_max = 0.5 * data_size(1) * max(abs(S{1}(:)));
lambda_max = 0;
for i = 2:num_time
    rho_max = max(rho_max, 0.5 * data_size(i) * max(abs(S{i}(:))));
    lambda_max = max(lambda_max, 0.5 * max(data_size(i), data_size(i-1))...
        * max(abs(S{i}(:) - S{i-1}(:))));
end

% Calculating Sample Inverse Covariance
S_train = cell(1, num_time);
InvCovMat = cell(1, num_time);
for i = 1:num_time
    Data{i} = zscore(Data{i});
    S_train{i} = cov(Data{i});
    InvCovMat{i} = inv(S_train{i});
end

% Variance Set
V_diag = zeros(num_time, NumVar);
V_offdiag = cell(num_time, NumVar);
for i = 1:num_time
    for j = 1:NumVar
        D = det(InvCovMat{i}) / det(InvCovMat{i}(setdiff(1:NumVar, j),...
            setdiff(1:NumVar, j)));
        V_diag(i,j) = D^2;
        D11 = InvCovMat{i}(setdiff(1:NumVar, j), setdiff(1:NumVar, j));
        D12 = InvCovMat{i}(setdiff(1:NumVar, j), j);
        V_offdiag{i, j} = inv((-2 * (D11 \ D12) * (D12' / D11)) / (D^2) ...
            + 2 * inv(D11) / D);
        [~, p] = chol(V_offdiag{i, j});
        if p ~= 0 
            [V, D] = eig(V_offdiag{i, j});
            V_offdiag{i, j} = V * abs(D) * V';
        end
    end
end

randCnt = 50000;
pdfMdl = cell(randCnt, 3);

% Random Seed
lambda_new =  exp(unifrnd(log(lambda_min), log(lambda_max)));
rho_new = exp(unifrnd(log(rho_min), log(rho_max)));
CovNew = InvCovMat;

m = 5;
for i = 1:1:randCnt
    fprintf('Iteration - %d\n',i);
    % Sampling Sparsity Parameter
    r = rho_new; 
    l = lambda_new;
    % Variance Calculation - Double Differentiation of log of constant 
    % factor with respect to parameter
    V = (r^2 * (12 * r^5 + 60 * r^4 * l + 109 * r^3 * l^2 + 89 * r^2 * l^3 ...
        + 32 * r * l^4 + 4 * l^5)^2) / (432 * r^10 + 3456 * r^9 * l ...
        + 12024 * r^8 * l^2 + 23760 * r^7 * l^3 + 29555 * r^6 * l^4 ...
        + 24554 * r^5 * l^5 + 14209 * r^4 * l^6 + 5816 * r^3 * l^7 ...
        + 1608 * r^2 * l^8 + 256 * r * l^9 + 16 * l^10);
    b = V / rho_new;
    a = rho_new / b;
    W = gamrnd(a, b);
    while W > rho_max
        W = gamrnd(a, b);
    end
    
    L = 0;
    for k = 1:num_time
        L =  L + 0.5 * data_size(k) * (log(det(CovNew{k})) ...
            - trace(CovNew{k} * S_train{k}) - NumVar * log(2 * pi));
        if k > 1
            L = L - sum(sum((0.5 * lambda_new * (1 + eye(NumVar,...
                NumVar))).*abs(CovNew{k} - CovNew{k-1})));
        end
    end
    L_old = L; 
    L_new=L;
    for k = 1:num_time
        L_old = L_old - sum(sum((0.5 * rho_new * (1 + eye(NumVar,...
            NumVar))).*abs(CovNew{k})));
        L_new = L_new - sum(sum((0.5 * W * (1 + eye(NumVar,...
            NumVar))).*abs(CovNew{k})));
    end
    priorConst = 1 * (0.5 * NumVar * (NumVar-1) + NumVar) * double(log(...
        subs(subs(sol3, 'a', rho_new), 'b', lambda_new)));
    L_old = L_old + priorConst;
    priorConst = 1 * (0.5 * NumVar * (NumVar - 1) + NumVar) * double(...
        log(subs(subs(sol3, 'a', W), 'b', lambda_new)));
    L_new = L_new + priorConst;
    
    r = W; 
    l = lambda_new;
    V = (r^2 * (12 * r^5 + 60 * r^4 * l + 109 * r^3 * l^2 + 89 * r^2 * l^3 ...
        + 32 * r * l^4 + 4 * l^5)^2) / (432 * r^10 + 3456 * r^9 * l ...
        + 12024 * r^8 * l^2 + 23760 * r^7 * l^3 + 29555 * r^6 * l^4 ...
        + 24554 * r^5 * l^5 + 14209 * r^4 * l^6 + 5816 * r^3 * l^7 ...
        + 1608 * r^2 * l^8 + 256 * r * l^9 + 16 * l^10);
    b1 = V / r;
    a1 = W / b1;
    P = min(0, L_new - L_old + log(gampdf(rho_new, a1, b1)) ...
        - log(gampdf(W, a, b)));
    if P > log(rand)
        rho_new  = W;
    end

    % Sampling smoothing parameter
    r = rho_new; 
    l = lambda_new;
    V = (12 * r^5 + 60 * r^4 * l + 109 * r^3 * l^2 + 89 * r^2 * l^3 ...
        + 32 * r * l^4 + 4 * l^5)^2 / (672 * r^8 + 4344 * r^7 * l ...
        + 12064 * r^6 * l^2 + 18616 * r^5 * l^3 + 17202 * r^4 * l^4 ...
        + 9584 * r^3 * l^5 + 3088 * r^2 * l^6 + 512 * r * l^7 ...
        + 32 * l^8);
    b = V / lambda_new;
    a = lambda_new / b;
    W = gamrnd(a, b);
    while W > lambda_max
        W = gamrnd(a, b);
    end
    
    L = 0;
    for k = 1:num_time
        L =  L + 0.5 * data_size(k) * (log(det(CovNew{k})) - trace(...
            CovNew{k} * S_train{k}) - NumVar * log(2 * pi));
        L = L - sum(sum((0.5 * rho_new * (1 + eye(NumVar, NumVar...
            ))).*abs(CovNew{k})));
    end
    L_old = L; 
    L_new=L;
    for k = 2:num_time
        L1 = CovNew{k} - CovNew{k-1};
        L_old = L_old - sum(sum((0.5 * lambda_new * (1 + eye(NumVar,...
            NumVar))).*abs(L1)));
        L_new = L_new - sum(sum((0.5 * W * (1 + eye(NumVar, ...
            NumVar))).*abs(L1)));
    end
    priorConst = 1 * (0.5 * NumVar * (NumVar - 1) + NumVar) * double(log(...
        subs(subs(sol3, 'a', rho_new), 'b', lambda_new)));
    L_old = L_old + priorConst;
    priorConst = 1 * (0.5 * NumVar * (NumVar - 1) + NumVar) * double(log(...
        subs(subs(sol3, 'a', rho_new), 'b', W)));
    L_new = L_new + priorConst;
    
    r = rho_new; 
    l = W;
    V = (12 * r^5 + 60 * r^4 * l + 109 * r^3 * l^2 + 89 * r^2 * l^3 ...
        + 32 * r * l^4 + 4 * l^5)^2 / (672 * r^8 + 4344 * r^7 * l ...
        + 12064 * r^6 * l^2 + 18616 * r^5 * l^3 + 17202 * r^4 * l^4 ...
        + 9584 * r^3 * l^5 + 3088 * r^2 * l^6 + 512 * r * l^7 + 32 * l^8);
    b1 = V / W;
    a1 = W / b1;
    P = min(0, L_new - L_old + log(gampdf(lambda_new, a1, b1)) ...
        - log(gampdf(W, a, b)));
    if P > log(rand)
        lambda_new = W;
    end
    
    % Sampling Inverse Covariance Matrix
    for t = 1:num_time
        for j = 1:NumVar
            % Sampling Diagonal Elements
            clear W W1 L L1 P
            W = zeros(1, m);
            L = zeros(1, m);
            for k = 1:m
                W(k) = normrnd(CovNew{t}(j, j), V_diag(t, j));
                C = CovNew;
                C{t}(j,j) = W(k);
                L(k) = 0;
                for k1 = 1:num_time
                    L(k) =  L(k) + 0.5 * data_size(k1) * (log(det(C{k1})) ...
                        - trace(C{k1} * S_train{k1}) - NumVar * log(2 * pi)); 
                    L(k) = L(k) - sum(sum((0.5 * rho_new * (1 + eye(...
                        NumVar, NumVar))).*abs(C{k1})));
                    if k1>1
                        L2 = C{k1} - C{k1-1};
                        L(k) = L(k)- sum(sum((0.5 * lambda_new * (1 ...
                            + eye(NumVar, NumVar))).*abs(L2)));
                    end
                end
                
                while ~isreal(L(k))
                    W(k) = normrnd(CovNew{t}(j,j), V_diag(t,j));
                    C = CovNew;
                    C{t}(j,j) = W(k);
                    
                    L(k) = 0;
                    for k1 = 1:num_time
                        L(k) =  L(k) + 0.5 * data_size(k1) * (log(det(...
                            C{k1})) - trace(C{k1} * S_train{k1}) ...
                            - NumVar * log(2 * pi));
                        L(k) = L(k) - sum(sum((0.5 * rho_new * (1 + eye(...
                            NumVar, NumVar))).*abs(C{k1})));
                        if k1 > 1
                            L2 = C{k1} - C{k1-1};
                            L(k) = L(k)- sum(sum((0.5 * lambda_new * (1 ...
                                + eye(NumVar, NumVar))).*abs(L2)));
                        end
                    end
                end
            end
            L_star = L - max(L);
            W_star = randsample(W, 1, true, exp(L_star));
            W1 = zeros(1, m);
            L1 = zeros(1, m);
            for k = 1:m
                if k == m
                    W1(k) = CovNew{t}(j,j);
                else
                    W1(k) = normrnd(W_star, V_diag(t,j));
                end
                C = CovNew;
                C{t}(j,j) = W1(k);
                
                L1(k) = 0;
                for k1 = 1:num_time
                    L1(k) =  L1(k) + 0.5 * data_size(k1) * (log(...
                        det(C{k1})) - trace(C{k1} * S_train{k1}) ...
                        - NumVar * log(2 * pi)); 
                    L1(k) = L1(k) - sum(sum((0.5 * rho_new * (1 ...
                        + eye(NumVar, NumVar))).*abs(C{k1})));
                    if k1 > 1
                        L2 = C{k1} - C{k1-1};
                        L1(k) = L1(k)- sum(sum((0.5 * lambda_new * (1 ...
                            + eye(NumVar, NumVar))).*abs(L2)));
                    end
                end
                
                while ~isreal(L1(k))
                    if k == m
                        W1(k) = CovNew{t}(j,j);
                    else
                        W1(k) = normrnd(W_star, V_diag(t,j));
                    end
                    C = CovNew;
                    C{t}(j,j) = W1(k);
                    
                    L1(k) = 0;
                    for k1 = 1:num_time
                        L1(k) =  L1(k) + 0.5 * data_size(k1) * (log(...
                            det(C{k1})) - trace(C{k1} * S_train{k1}) ...
                            - NumVar * log(2 * pi));
                        L1(k) = L1(k) - sum(sum((0.5 * rho_new * (1 ...
                            + eye(NumVar, NumVar))).*abs(C{k1})));
                        if k1 > 1
                            L2 = C{k1} - C{k1-1};
                            L1(k) = L1(k)- sum(sum((0.5 * lambda_new * (1 ...
                                + eye(NumVar, NumVar))).*abs(L2)));
                        end
                    end
                end
            end
            
            P = min(1, sum(exp(L - max([L, L1]))) / sum(exp(L1 ...
                - max([L, L1]))));
            if P > rand
                CovNew{t}(j,j) = W_star;
            end
            clear W W1 L L1 P
            
            % Sampling Off diagonal vectors
            W = cell(1, m);
            L = zeros(1, m);
            for k = 1:m
                W{k} = mvnrnd(CovNew{t}(setdiff(1:NumVar, j), j),...
                    V_offdiag{t,j});
                C = CovNew;
                C{t}(setdiff(1:NumVar, j), j) = W{k}';
                C{t}(j, setdiff(1:NumVar, j)) = W{k};
                
                L(k) = 0;
                for k1 = 1:num_time
                    L(k) =  L(k) + 0.5 * data_size(k1) * (log(det(C{k1})) ...
                        - trace(C{k1} * S_train{k1}) - NumVar * log(2 * pi));
                    L(k) = L(k) - sum(sum((0.5 * rho_new * (1 + eye(...
                        NumVar, NumVar))).*abs(C{k1})));
                    if k1 > 1
                        L2 = C{k1} - C{k1-1};
                        L(k) = L(k)- sum(sum((0.5 * lambda_new * (1 ...
                            + eye(NumVar, NumVar))).*abs(L2)));
                    end
                end
              
                while ~isreal(L(k))
                    W{k} = mvnrnd(CovNew{t}(setdiff(1:NumVar, j), j),...
                        V_offdiag{t,j});
                    C = CovNew;
                    C{t}(setdiff(1:NumVar, j), j) = W{k}';
                    C{t}(j, setdiff(1:NumVar, j)) = W{k};
                    
                    L(k) = 0;
                    for k1 = 1:num_time
                        L(k) =  L(k) + 0.5 * data_size(k1) * (log(...
                            det(C{k1})) - trace(C{k1} * S_train{k1}) ...
                            - NumVar * log(2 * pi));
                        L(k) = L(k) - sum(sum((0.5 * rho_new * (1 ...
                            + eye(NumVar, NumVar))).*abs(C{k1})));
                        if k1 > 1
                            L2 = C{k1} - C{k1-1};
                            L(k) = L(k)- sum(sum((0.5 * lambda_new * (1 ...
                                + eye(NumVar, NumVar))).*abs(L2)));
                        end
                    end
                end
            end
            L_star = L - max(L);
            W_star = W{randsample(1:m, 1, true, exp(L_star))};
            W1 = cell(1, m);
            L1 = zeros(1, m);
            for k = 1:m
                if k == m
                    W1{k} = CovNew{t}(setdiff(1:NumVar, j), j)';
                else
                    W1{k} = mvnrnd(W_star, V_offdiag{t,j});
                end
                C = CovNew;
                C{t}(setdiff(1:NumVar,j), j) = W1{k}';
                C{t}(j, setdiff(1:NumVar,j)) = W1{k};
                
                L1(k) = 0;
                for k1 = 1:num_time
                    L1(k) =  L1(k) + 0.5 * data_size(k1) * (log(...
                        det(C{k1})) - trace(C{k1} * S_train{k1}) ...
                        - NumVar * log(2 * pi));
                    L1(k) = L1(k) - sum(sum((0.5 * rho_new * (1 ...
                        + eye(NumVar, NumVar))).*abs(C{k1})));
                    if k1 > 1
                        L2 = C{k1} - C{k1-1};
                        L1(k) = L1(k)- sum(sum((0.5 * lambda_new * (1 ...
                            + eye(NumVar, NumVar))).*abs(L2)));
                    end
                end
              
                while ~isreal(L1(k))
                    W1{k} = mvnrnd(W_star, V_offdiag{t,j});
                    C = CovNew;
                    C{t}(setdiff(1:NumVar,j), j) = W1{k}';
                    C{t}(j, setdiff(1:NumVar,j)) = W1{k};
                    
                    L1(k) = 0;
                    for k1 = 1:num_time
                        L1(k) =  L1(k) + 0.5 * data_size(k1) * (log(...
                            det(C{k1})) - trace(C{k1} * S_train{k1}) ...
                            - NumVar * log(2 * pi));
                        L1(k) = L1(k) - sum(sum((0.5 * rho_new * (1 ...
                            + eye(NumVar, NumVar))).*abs(C{k1})));
                        if k1 > 1
                            L2 = C{k1} - C{k1-1};
                            L1(k) = L1(k)- sum(sum((0.5 * lambda_new * ...
                                (1 + eye(NumVar, NumVar))).*abs(L2)));
                        end
                    end
                end
            end
            P = min(1, sum(exp(L - max([L, L1]))) / sum(exp(L1 ...
                - max([L, L1]))));
            if P > rand
                CovNew{t}(setdiff(1:NumVar,j), j) = W_star';
                CovNew{t}(j, setdiff(1:NumVar,j)) = W_star;
            end
            clear W W1 L L1 P
        end
    end
    
    pdfMdl{i,1} = [rho_new, lambda_new];
    pdfMdl{i,2} = CovNew;
    
    L=0;
    for k = 1:num_time
        L =  L + 0.5 * data_size(k) * (log(det(CovNew{k})) ...
            - trace(CovNew{k} * S_train{k}) - NumVar * log(2 * pi));
        L = L - sum(sum((0.5 * rho_new * (1 + eye(NumVar,...
            NumVar))).*abs(CovNew{k})));
        if k > 1
            L2 = CovNew{k} - CovNew{k-1};
            L = L- sum(sum((0.5 * lambda_new * (1 + eye(NumVar,...
                NumVar))).*abs(L2)));
        end
    end
    priorConst = 1 * (0.5 * NumVar * (NumVar - 1) + NumVar) * double(log(...
        subs(subs(sol3, 'a', rho_new), 'b', lambda_new)));
    L = L + priorConst;
    pdfMdl{i,3} = L;
    if mod(i,100)==0
        save('Temp.mat','pdfMdl','-append');
    end
end