% DYNAMIC_GLASSO_BIC - Estimate inverse covariance matrices using dynamic
%   GLASSO
% This function finds the optimal solution of dynamic GLASSO model at given
%   parameter range using extended BIC (Bayesian Information Criterion).
%
% Syntax: [sol, rhomax, lambdamax, loglike, avg_time] =
%           dynamic_glasso_bic(data, rhopath, lambdapath, edge_threshold)
%
% Inputs:
%   data - temporal data cell array where each cells has data matrix
%           corresponding to single time point
%   rhopath - a grid of sparsity parameter values from which the
%         optimal one is chosen
%   lambdapath - a grid of smoothing parameter values from which the
%         optimal one is chosen
%   edge_threshold - threshold to remove edges from the graph
%
% Outputs:
%   sol - estimated inverse covariance matrices under optimal parameters
%   rhomax - optimal sparsity parameter among rhopath
%   lambdamax - optimal smoothing parameter among lambdapath
%   loglike - log-likelihood
%   avg_time - average computation time
%
% Other m-files required: Yalmip package
% Subfunctions: none
% MAT-files required: priorConst.mat

% Author: Sunil Kumar, Ph.D., ETH Zurich
% email address: sunil.kumar@alumni.ethz.ch
% Website: https://www.bcs.tu-darmstadt.de/
% June, 2016

%------------------------- BEGIN CODE -------------------------------------
function [sol, rhomax, lambdamax, loglike, avg_time] = dynamic_glasso_bic(...
    data, rhopath, lambdapath, edge_threshold)

% Get data information
num_time = length(data);
num_var = size(data{1}, 2);
data_size = zeros(num_time, 1);
for i = 1:num_time
    data_size(i) = size(data{i}, 1);
end

% Loading prior Constant function
pCons = eval(sprintf('load(''priorConst.mat'',''sol%d'');', num_time));
if isempty(fieldnames(pCons))
    IC = 1;         % log issue log(1) = 0
else
    eval(sprintf('IC = pCons.sol%d;', num_time));
end

% Extended Bayesian Information Criterion
loglike = zeros(length(rhopath), length(lambdapath));
S_train = cell(num_time, 1);
A = cell(num_time, 1);
avg_time = zeros((length(rhopath) * length(lambdapath)), 1);
for j = 1:num_time
    S_train{j} = cov(data{j}, 1);
    % Check the standardization
    if abs(sum(diag(S_train{j}) - ones(num_var, 1))) < 1e-6
        A{j} = eye(num_var);
    else
        A{j} = diag(1./sqrt(diag(inv(S_train{j}))));
    end
end
parfor k = 1:(length(rhopath) * length(lambdapath))
    tic;
    X = cell(num_time, 1);
    num_edges = zeros(num_time, 1);
    constraints = [];
    for j = 1:num_time
        X{j} = sdpvar(num_var, num_var);
        constraints = constraints + (X{j}>=0);
    end
    
    sol = cell(num_time, 1);
    lambda_index = ceil(k / length(rhopath));
    rho_index = mod(k, length(rhopath));
    if rho_index == 0
        rho_index = length(rhopath);
    end
    rho = rhopath(rho_index) * (1 + eye(num_var, num_var));
    lambda = lambdapath(lambda_index) * (1 + eye(num_var, num_var));  %#ok<PFBNS>
    rho1 = ones(num_var, num_var, num_time);
    
    objective = 0;
    for j = 1:num_time
        objective = objective - 0.5 * data_size(j) * (logdet(X{j}) ...
            - trace(X{j} * S_train{j})) + 0.5 * sum(sum(rho.*abs(...
            A{j} * X{j} * A{j}))); %#ok<PFBNS>
        if j >= 2
            L = lambda.*abs(A{j} * X{j} * A{j} - A{j-1} * X{j-1} * A{j-1});
            objective = objective + 0.5 * sum(L(:));
        end
    end
    solvesdp(constraints, objective, sdpsettings('solver', 'sdpt3',...
        'verbose', 0, 'sdpt3.maxit', 100, 'sdpt3.gaptol', 1e-10,...
        'debug', 1));
    for j = 1:num_time
        sol{j}= double(X{j});
        % upper triangular nonzero count
        num_edges(j) = 0.5 * sum(abs(sol{j}(logical(ones(num_var, num_var)...
            - eye(num_var)))) > edge_threshold) + num_var;
        rho1(:,:,j) = 1e+20 * (abs(sol{j}) < edge_threshold);
    end
    
    objective = 0;
    for j = 1:num_time
        objective = objective - 0.5 * data_size(j) * (logdet(X{j}) ...
            - trace(X{j} * S_train{j})) + 0.5 * sum(sum(rho1(:,:,j).*abs(...
            A{j} * X{j} * A{j})));
        if j >= 2
            L = lambda.*abs(A{j} * X{j} * A{j} - A{j-1} * X{j-1} * A{j-1});
            objective = objective + 0.5 * sum(L(:));
        end
    end
    solvesdp(constraints, objective, sdpsettings('solver', 'sdpt3',...
        'verbose', 0, 'sdpt3.maxit', 100, 'sdpt3.gaptol', 1e-10,...
        'usex0', 0, 'debug', 1));
    for j = 1:num_time
        sol{j}= double(X{j});
    end
    yalmip('clear');
    
    % Calculation BIC
    L = 0;
    for j = 1:num_time
        L =  L - 0.5 * data_size(j) * (log(det(sol{j})) ...
            - trace(sol{j} * S_train{j}) - num_var * log(2 * pi));
        L1 = rho.*abs(A{j} * sol{j} * A{j});
        L = L + 0.5 * sum(L1(:));
        if j > 1
            L1 = abs(A{j} * sol{j} * A{j} - A{j-1} * sol{j-1} * A{j-1});
            L = L + 0.5 * sum(sum(lambda.*L1));
        end
    end
    L = L + 0.5 * sum((log(data_size).*num_edges)) - 0.5 * sum((log(2 ...
        * pi) * num_edges));
    
    r1 = rhopath(rho_index); 
    r2 = lambdapath(lambda_index);
    priorConst = -1 * (0.5 * num_var * (num_var-1) + num_var) ...
        * double(log(subs(subs(IC,'a',r1),'b',r2)));
    L = L + priorConst;
    
    loglike(k) = L;
    avg_time(k) = toc;
    yalmip('clear');
    fprintf('rho: %d/%d, lambda: %d/%d, Time Taken - %d\n', rho_index,...
        length(rhopath), lambda_index, length(lambdapath), avg_time(k));
end

[a, b] = find(loglike==min(min(loglike)));
rhomax = rhopath(a(end));
lambdamax = lambdapath(b(end));

disp('Solution at desired regularized parameter...');
yalmip('clear');
sol = cell(num_time, 1);
constraints = [];
X = cell(num_time, 1);
rho = rhomax * (1 + eye(num_var, num_var));  
lambda = lambdamax * (1 + eye(num_var, num_var));
for j=1:num_time
    S_train{j} = cov(data{j}, 1);
    if sum(diag(S_train{j}) - ones(num_var, 1)) < 1e-6
        A{j} = eye(num_var);
    else
        A{j} = diag(1./sqrt(diag(inv(S_train{j}))));
    end
    X{j} = sdpvar(num_var, num_var);
    constraints = constraints + (X{j} >= 0);
end

objective = 0;
rho1 = ones(num_var, num_var, num_time);

for j = 1:num_time
    objective = objective - 0.5 * data_size(j) * (logdet(X{j}) ...
        - trace(X{j} * S_train{j})) + 0.5 * sum(sum(rho.*abs(...
        A{j} * X{j} * A{j}))); 
    if j >= 2
        L = lambda.*abs(A{j} * X{j} * A{j} - A{j-1} * X{j-1} * A{j-1}); 
        objective = objective + 0.5 * sum(L(:));                          
    end
end
solvesdp(constraints, objective, sdpsettings('solver', 'sdpt3',...
    'verbose', 0, 'sdpt3.maxit', 100, 'sdpt3.gaptol', 1e-10, 'debug', 1));
for j = 1:num_time
    sol{j}= double(X{j});
    rho1(:,:,j) = 1e+20 * (abs(sol{j}) < edge_threshold);
end

objective = 0;
for j = 1:num_time
    objective = objective - 0.5 * data_size(j) * (logdet(X{j}) ...
        - trace(X{j} * S_train{j})) + 0.5 * sum(sum(rho1(:,:,j).*abs(...
        A{j} * X{j} * A{j})));
    if j >= 2
        L = lambda.*abs(A{j} * X{j} * A{j} - A{j-1} * X{j-1} * A{j-1});    
        objective = objective + 0.5 * sum(L(:));                      
    end
end
solvesdp(constraints, objective, sdpsettings('solver', 'sdpt3',...
    'verbose', 0, 'sdpt3.maxit', 100, 'sdpt3.gaptol', 1e-10, 'debug', 1));
for j = 1:num_time
    sol{j} = double(X{j});
end
avg_time = mean(avg_time);
end

%------------------------ END OF CODE -----------------------------------