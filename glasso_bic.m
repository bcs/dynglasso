% GLASSO_BIC - Estimate inverse covariance matrices using GLASSO
% This function finds the optimal solution of GLASSO model at given
%   parameter range using BIC (Bayesian Information Criterion).
%
% Syntax: [W, M, rhomax, bic] =
%           glasso_bic(data, rhopath, edge_threshold, bic_method)
%
% Inputs:
%   data -  p by n data matrix where dim = p, sample size = n
%   rhopath - a grid of shrinkage parameter values from which the
%         optimal one is chosen
%   edge_threshold - Threshold to remove edges from the graph
%   bic_method - two approaches to calculate BIC (default=1)
%
% Outputs:
%   W - estimated covariance matrix under optimal shrinkage parameter
%   M - estimated precision matrix under optimal shrinkage parameter
%   rhomax - optimal shrinkage parameter among rhopath
%   bic - BIC values for different shrinkage parameter
%
% Other m-files required: glasso_FTH (BGlassoPackage)
% Subfunctions: none
% MAT-files required: none

% Author: Sunil Kumar, Ph.D., ETH Zurich
% email address: sunil.kumar@alumni.ethz.ch
% Website: https://www.bcs.tu-darmstadt.de/
% June, 2016

%------------------------- BEGIN CODE -------------------------------------
function [W, M, rhomax, bic] = glasso_bic(data, rhopath, ...
                        edge_threshold, bic_method)
                    
if ~exist('bic_method','var')
    bic_method = 1;
end

[num_var, n_train] = size(data);

bic = zeros(1, length(rhopath));
S_train = data * data';

for j = 1:length(rhopath)
 fprintf('j = %d\n', j);
 rho = rhopath(j);   
 [~, M] = glasso_FTH(S_train/n_train, rho);
 num_edges = 0.5 * sum(abs(M(logical(ones(num_var, num_var)...
     - eye(num_var)))) > edge_threshold) + num_var;
 rho = 1e+20 * (abs(M) < edge_threshold);
 
 [~, M] = glasso_FTH(S_train/n_train, rho);
 if bic_method == 1
    bic(j) = -log(det(M)) + trace(S_train/n_train * M)...
        + log(n_train) * num_edges / n_train;
 elseif bic_method == 2
    bic(j) = - 0.5 * n_train * (log(det(M)) - trace(M * S_train) ...
        - num_var * log(2 * pi)) + 0.5 * sum((log(n_train).*num_edges)) ...
        - 0.5 * sum((log(2 * pi) * num_edges)) + sum(rhopath(j) ...
        * abs(M(:))) - (num_var * num_var) * log(0.5 * rhopath(j)); 
 end
end
[~, b] = min(bic);
rhomax = rhopath(b);
[~, M] = glasso_FTH(S_train / n_train, rhomax);
rho = 1e+20 * (abs(M) < edge_threshold);
[W, M] = glasso_FTH(S_train / n_train, rho);

%------------------------ END OF CODE -----------------------------------
